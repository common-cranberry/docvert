
import { Component } from "@angular/core";
import { FormControl, FormGroupDirective,
  NgForm, Validators } from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material";
import ItemService from "../ItemService";
import "./ConvertStyle.scss";
import ConvertTemplate = require("./ConvertTemplate.pug");

export class DirtyStateMatcher implements ErrorStateMatcher {
  public isErrorState (
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    return !!(
      control && control.invalid &&
        (control.dirty || control.touched || !!(form && form.submitted)));
  }
}

// tslint:disable-next-line
@Component({
  selector: "page-convert",
  template: ConvertTemplate()
})
export default class ConvertComponent {
  private service: ItemService;
  private link: FormControl;
  private email: FormControl;
  private password: FormControl;
  private matcher: ErrorStateMatcher;

  public constructor ( service: ItemService ) {
    this.service = service;
    this.matcher = new DirtyStateMatcher();
    this.link = new FormControl("", [
      Validators.required
    ]);
    this.email = new FormControl("");
    this.password = new FormControl("");
  }

  public getService ( ): ItemService {
    return this.service;
  }

  public getMatcher ( ): ErrorStateMatcher {
    return this.matcher;
  }

  public getLinkHint ( ): string {
    const match: any = this.link.value.match(
      /^(?:https?:\/\/)?(?:docsend)?(?:.com\/?(?:view\/)?|:)?(.+)$/
    );
    return match ? `https://docsend.com/view/${match[1]}` : "";
  }

  public requestAddItem ( ): void {
    this.service.requestCreateItem({
      link: this.getLinkHint(),
      email: this.email.value,
      passcode: this.password.value
    });
    this.link.setValue("");
  }
}
