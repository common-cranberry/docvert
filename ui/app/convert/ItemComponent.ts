
import { Component, Input } from "@angular/core";
import ItemService, { ItemWrapper } from "../ItemService";
import "./ItemStyle.scss";
import ItemTemplate = require("./ItemTemplate.pug");

@Component({
  selector: "convert-item",
  template: ItemTemplate()
})
export default class ItemComponent {
  @Input() public wrap: ItemWrapper;
  @Input() public service: ItemService;

  public getTooltipForStep ( ): string {
    switch (this.wrap.data.step) {
      case 1: return "Locating";
      case 2: return "Retrieving";
      case 3: return "Converting";
      case 4: return "Complete";
      default: return "";
    }
  }

  public getIconForState ( state: string ): string {
    switch (state) {
      case "COMPLETE": return "fa-check-circle";
      case "ERROR": return "fa-times-circle";
      case "PENDING": return "fa-circle-notch";
      default: return "fa-circle";
    }
  }

  public getIconSetForState ( state: string ): string {
    switch (state) {
      case "PENDING": return "fas";
      default: return "far";
    }
  }

  public getSpinForState ( state: string ): boolean {
    switch (state) {
      case "PENDING": return true;
      default: return false;
    }
  }
}
