
import { APP_BASE_HREF, CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
// import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";

import AppComponent from "./AppComponent";
import MaterialModule from "./MaterialModule";

import AboutComponent from "./about/AboutComponent";
import AuthComponent from "./auth/AuthComponent";
import ConvertComponent from "./convert/ConvertComponent";
import ItemComponent from "./convert/ItemComponent";

@NgModule({
  providers: [ {
    provide: APP_BASE_HREF,
    useValue: ""
  } ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MaterialModule,
    // NoopAnimationsModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      { path: "about", component: AboutComponent },
      { path: "auth", component: AuthComponent },
      { path: "convert", component: ConvertComponent },
      { path: "", redirectTo: "/convert", pathMatch: "full" }
    ])
  ],
  declarations: [
    AppComponent,
    AboutComponent,
    AuthComponent,
    ConvertComponent,
    ItemComponent
  ],
  bootstrap: [ AppComponent ]
})
export default class AppModule { }
