
import { Injectable } from "@angular/core";
import { MatSnackBar, MatSnackBarRef } from "@angular/material";
import Fingerprintjs2 from "fingerprintjs2";
import { BehaviorSubject, Observable, ReplaySubject } from "rxjs";
import { map } from "rxjs/operators";
import io from "socket.io-client";

// TODO: split client out of service

export enum ItemStep {
  LOCATE   = 1,
  RETRIEVE = 2,
  CONVERT  = 3,
  DONE     = 4
}

export type ItemData = {
  name: string;
  // 0-1: progress
  progress: number;
  // 1: locate, 2: retrieve, 3: convert, 4: done
  step: number;
  error?: string;
};

export type ItemMetaStep = {
  name: string;
  state: string;
};

export type ItemMetaSteps =
  Array<ItemMetaStep>;

export type ItemMeta = {
  state: string;
  steps: ItemMetaSteps;
  hasProgress: boolean;
  hasError: boolean;
  hasSource: boolean;
  hasConvert: boolean;
};

export type ItemWrapper = {
  ref: string;
  meta: ItemMeta;
  data: ItemData;
};

const STEPS: Array<string> = [ "Locate", "Retrieve", "Convert" ];

function getState ( item: ItemData ): string {
  if (item.error) { return "ERROR"; }
  if (item.step < ItemStep.DONE) { return "PENDING"; }
  return "COMPLETE";
}

function getStateForStep ( item: ItemData, step: number ): string {
  if (step < item.step) { return "COMPLETE"; }
  if (step > item.step) { return "NOT_STARTED"; }
  return getState(item);
}

function getItemMeta ( item: ItemData ): ItemMeta {
 return {
    state: getState(item),
    steps: STEPS.map(function ( name: string, index: number ): ItemMetaStep {
      return { name: name, state: getStateForStep(item, index + 1) };
    }),
    hasProgress: item.step !== ItemStep.DONE,
    hasError: !!item.error,
    hasSource: item.step > ItemStep.RETRIEVE,
    hasConvert: item.step > ItemStep.CONVERT
  };
}

@Injectable({ providedIn: "root" })
export default class ItemService {
  private snackbar: MatSnackBar;
  private refs: BehaviorSubject<Array<string> | null>;
  private items: { [key: string]: BehaviorSubject<ItemWrapper> };
  private requests: ReplaySubject<any>;
  private fingerprint: string;

  public constructor ( snackbar: MatSnackBar ) {
    this.snackbar = snackbar;
    this.refs = new BehaviorSubject<Array<string> | null>(null);
    this.items = { };
    this.requests = new ReplaySubject();
    this.init();
  }

  public getRefs ( ): Observable<Array<string> | null> {
    return this.refs.asObservable();
  }

  public getItems ( ): Observable<Array<Observable<ItemWrapper>>> {
    return this.getRefs().pipe(map((
      refs: Array<string> | null
    ): Array<Observable<ItemWrapper>> => {
      if (refs === null) { return [ ]; }
      return refs.map(( ref: string ): Observable<ItemWrapper> => {
        return this.items[ref].asObservable();
      });
    }));
  }

  public requestCreateItem ( req: {
    link: string, email: string, passcode: string
  } ): void {
    this.requests.next({ type: "create", data: req });
    this.emit("Accepted for processing: " + req.link);
  }

  public requestRemoveItem ( wrap: ItemWrapper ): void {
    this.requests.next({ type: "delete", data: wrap.ref });
    this.emit("Requesting removal: " + wrap.data.name);
  }

  public useMockItems ( delay: number = 1000 ): void {
    const updates: Array<ItemData> = [ {
      name: "Test Pending Locate",
      progress: 0.4,
      step: 1
    }, {
      name: "Test Pending Retrieve",
      progress: 0.2,
      step: 2
    }, {
      name: "Test Pending Convert",
      progress: 0.7,
      step: 3
    }, {
      name: "Test Error Locate",
      progress: -1,
      step: 1,
      error: "Mock Error"
    }, {
      name: "Test Error Retrieve",
      progress: -1,
      step: 2,
      error: "Mock Error"
    }, {
      name: "Test Error Convert",
      progress: -1,
      step: 3,
      error: "Mock Error"
    }, {
      name: "Test Complete",
      progress: 1,
      step: 4
    } ];
    let index: number = 0;
    const v: any = setInterval(( ): void => {
      this.addItem(index + "", updates[index++]);
      if (index >= updates.length) {
        clearInterval(v);
      }
    }, delay);
  }

  private addItem ( ref: string, item: ItemData ): void {
    this.items[ref] = new BehaviorSubject({
      ref: ref, data: item, meta: getItemMeta(item)
    });
  }

  private updateItem ( ref: string, item: ItemData ): void {
    this.items[ref].next({
      ref: ref, data: item, meta: getItemMeta(item)
    });
  }

  private removeItem ( ref: string ): void {
    const refs: Array<string> = this.refs.value || [ ];
    refs.splice(refs.indexOf(ref), 1);
    this.refs.next(refs);
    delete this.items[ref];
  }

  private init ( ): void {
    new Promise<string>(( resolve: any ): void => {
      if (localStorage.hasOwnProperty("fingerprint")) {
        resolve(localStorage.getItem("fingerprint"));
      } else {
        const ref: MatSnackBarRef<any> = this.emit(
          "Please allow a moment for fingerprinting", Infinity);
        setTimeout(( ): void => {
          new Fingerprintjs2().get(function ( fingerprint: string ): void {
            localStorage.setItem("fingerprint", fingerprint);
            ref.dismiss();
            resolve(fingerprint);
          });
        }, 10);
      }
    }).then(( fingerprint: string ): SocketIOClient.Socket => {
      const socket: SocketIOClient.Socket = io(
        process.env.NODE_SERVER_LOCATION ||
        `http://${document.domain}:${location.port}/socket.io`);
      this.setup(socket);
      this.fingerprint = fingerprint;
      socket.emit("fingerprint", this.fingerprint);
      this.requests.subscribe(function ( request: any ): void {
        socket.emit(request.type, request.data);
      });
      return socket;
    });
  }

  private setup ( socket: SocketIOClient.Socket ): void {
    socket.on("all", ( data: { [key: string]: ItemData }): void => {
      const refs: Array<string> = [ ];
      for (const ref in data) {
        refs.push(ref);
        this.addItem(ref, data[ref]);
      }
      this.refs.next(refs);
    });
    socket.on("create", ( data: { [key: string]: ItemData }): void => {
      for (const ref in data) {
        this.addItem(ref, data[ref]);
        this.refs.next((this.refs.value || [ ]).concat([ ref ]));
      }
    });
    socket.on("update", ( data: { [key: string]: ItemData }): void => {
      for (const ref in data) {
        this.updateItem(ref, data[ref]);
      }
    });
    socket.on("delete", ( data: { [key: string]: ItemData }): void => {
      for (const ref in data) {
        this.removeItem(ref);
      }
    });
    socket.on("error", ( data: string ): void => {
      this.emit(data);
    });
    socket.on("fingerprint", ( ): void => {
      socket.emit("fingerprint", this.fingerprint);
    });
  }

  private emit (
    message: string, duration: number = 3000
  ): MatSnackBarRef<any> {
    return this.snackbar.open(message, undefined, { duration });
  }
}
