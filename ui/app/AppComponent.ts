
import { Component } from "@angular/core";
import "./AppStyle.scss";
import AppTemplate = require("./AppTemplate.pug");

@Component({
  selector: "app",
  template: AppTemplate()
})
export default class AppComponent { }
