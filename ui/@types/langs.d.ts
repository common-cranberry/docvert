
declare module "*.pug" {
  function template ( data?: any ): string;
  export = template;
}
