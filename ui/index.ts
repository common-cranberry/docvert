
import "core-js/es7/reflect";
import "zone.js/dist/zone";

import "hammerjs";

import { enableProdMode } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import AppModule from "./app/AppModule";

setTimeout(function ( ): void {
  document.body.appendChild(document.createElement("app"));
  platformBrowserDynamic().bootstrapModule(AppModule);
});

if (process.env.NODE_ENV === "production") {
  enableProdMode();
}
