
import os
import uuid
import eventlet

from Processor import Processor

store = os.path.join(os.getcwd(), "_store")
if not os.path.exists(store):
  os.mkdir(store)

class Owner(object):
  def __init__(self, server):
    self.server = server
    self.room = str(uuid.uuid4())
    self.links = { }
    self.store = os.path.join(store, self.room)
    self.processors = { }
    if not os.path.exists(self.store):
      os.mkdir(self.store)

  def createProcessor(self, request):
    if request.link in self.links:
      return False
    id = str(uuid.uuid4())
    processor = self.processors[id] = Processor(self, id, request, self.store)
    self.links[request.link] = True
    self.broadcast("create", processor.getWrappedData())
    return True

  def deleteProcessor(self, id):
    if not id in self.processors:
      return False
    processor = self.processors[id]
    del self.links[processor.request.link]
    del self.processors[id]
    self.broadcast("delete", processor.getWrappedData())
    return True

  def register(self, sid):
    self.server.enter_room(sid, self.room, namespace="/socket.io")
    # no room, all data only needs to go to registering socket
    data={id: proc.getData() for id, proc in self.processors.items()}
    self.server.emit("all", data, namespace="/socket.io")

  def broadcast(self, type, data):
    self.server.emit(type, data, room=self.room, namespace="/socket.io")
    # give the primary thread enough room to send
    eventlet.sleep(0.1)
