
# server
from socketio import Server, Middleware
import eventlet
import eventlet.wsgi
from flask import Flask, send_from_directory
# system
import os

# TODO: break socket into class

from Connection import Connection
from RequestCreate import RequestCreate

server = Server()
app = Flask(__name__)

security_tokens = { "1": "2" }
connections = { }

static = os.path.join(os.getcwd(), "static")
_store = os.path.join(os.getcwd(), "_store")

@server.on("connect", namespace="/socket.io")
def connect(sid, environ):
  connection = connections[sid] = Connection(server, sid)
  connection.log("connect")

@server.on("disconnect", namespace="/socket.io")
def disconnect(sid):
  connection = connections[sid]
  connection.log("disconnect")
  del connections[sid]

@server.on("fingerprint", namespace="/socket.io")
def fingerprint(sid, fingerprint):
  connection = connections[sid]
  connection.log("fingerprint", fingerprint)
  connection.setFingerprint(fingerprint)

@server.on("create", namespace="/socket.io")
def create(sid, data):
  request = RequestCreate(data)
  connection = connections[sid]
  connection.log("create", request.link)
  connection.createProcessor(request)

@server.on("delete", namespace="/socket.io")
def delete(sid, id):
  connection = connections[sid]
  connection.log("delete", id)
  connection.deleteProcessor(id)

#@server.on("download", namespace="/socket.io")
##def download(sid, id)

@app.route("/_/<path:token>")
def retrieve(token):
  if token in security_tokens:
    location = security_tokens[token]
    del security_tokens[token]
    return location
  return "403 Forbidden - Security Exception - Invalid Token", 403

@app.route("/_store/<path:path>")
def download(path):
  return send_from_directory(_store, path)

@app.route("/", defaults={"path": "index.html"})
@app.route("/<path:path>")
def single(path):
  if not os.path.exists(os.path.join(static, path)):
    path = "index.html"
  ret = send_from_directory(static, path)
  return ret

if __name__ == "__main__":
  middle = Middleware(server, app)
  eventlet.wsgi.server(eventlet.listen(
    ("", int(os.getenv("PORT", 5001)))), middle)
