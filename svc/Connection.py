
from Owner import Owner

# TODO: put this somewhere safer than memory
owners = { }
# TODO: request fingerprint on timeout and reconnect

class Connection(object):

  def __init__(self, server, sid):
    self.server = server
    self.sid = sid
    self.sign = self.sid[-6:]

  def log(self, method, data=""):
    print("[socket][" + self.sign + "]/" + method,  data)

  def setFingerprint(self, fingerprint):
    if hasattr(self, "fingerprint"):
      self.server.emit("error", "Duplicate fingerprint, ignoring", namespace="/socket.io")
      return
    self.fingerprint = fingerprint
    if not self.fingerprint in owners:
      owners[self.fingerprint] = Owner(self.server)
    self.owner = owners[self.fingerprint]
    self.owner.register(self.sid)

  def validate(self):
    if not hasattr(self, "fingerprint"):
      self.server.emit("error", "Message received without fingerprint", namespace="/socket.io")
      self.server.emit("fingerprint", namespace="/socket.io")
      return False
    return True

  def createProcessor(self, request):
    if self.validate() and not self.owner.createProcessor(request):
      self.server.emit("error", "Already processing " + request.link, namespace="/socket.io")

  def deleteProcessor(self, id):
    if self.validate() and not self.owner.deleteProcessor(id):
      self.server.emit("error", "Delete failed, unknown identifier", namespace="/socket.io")
