
import eventlet
import os
import platform
import zipfile
from lxml import html
from PIL import Image, ImageChops
from fpdf import FPDF
from ProcessorState import ProcessorState

# def retrieveCompress(p):
#   p.path_raw = os.path.join(p.store, "raw.zip")
#   zf = zipfile.ZipFile(p.path_raw, "w")
#   for dirname, subdirs, files in os.walk(os.path.join(p.store, "raw")):
#     zf.write(dirname)
#     for filename in files:
#       zf.write(os.path.join(dirname, filename))
#     zf.close()

def runner(proc):
  eventlet.sleep(1)
  proc.state.doRun()
def step1_0(proc, state):
  return state.doStep(1, 3)
def step1_1(proc, state):
  res = state.session.get(proc.request.link, headers=state.headers)
  if not res.ok:
    return proc.error("Specified document could not be located")
  state.step1_tree1 = html.fromstring(res.content)
  return state.doProgress()
def step1_2(proc, state):
  tree = state.step1_tree1
  next = tree.xpath("//meta[@name='csrf-token']/@content")
  if len(next) != 1:
    return proc.error("Specified document is malformed, token not specified")
  state.step1_token = next[0]
  next = tree.xpath("//input[@id='link_auth_form_email']")
  state.step1_email = len(next) == 1
  next = tree.xpath("//input[@id='link_auth_form_passcode']")
  state.step1_passcode = len(next) == 1
  return state.doProgress()
def step1_3(proc, state):
  payload = {
    "_method":"patch",
    "authenticity_token": state.step1_token,
    "utf8":"✓"
  }
  if state.step1_email:
    payload["link_auth_form[email]"] = proc.request.email
  if state.step1_passcode:
    payload["link_auth_form[passcode]"] = proc.request.passcode
  res = state.session.post(proc.request.link, headers=state.headers, data=payload)
  if not res.ok:
    print(res)
    return proc.error("Specified document could not be located")
  tree = html.fromstring(res.content)
  invalid = tree.xpath("//form[@id='new_link_auth_form']")
  if len(invalid) != 0:
    return proc.error("Specified credentials are invalid")
  state.step1_pages = tree.xpath("//img[@class='preso-view page-view']/@data-url")
  return state.doProgress()
def step2_0(proc, state):
  state.doStep(2, len(state.step1_pages))
  state.step2_images = [ ]
  for num, page in enumerate(state.step1_pages, start=1):
    res = state.session.get(page, headers=state.headers)
    link = res.json()["imageUrl"]
    res = state.session.get(link, headers=state.headers)
    if res.ok:
      image = os.path.join(proc.raw, "page" + str(num) + ".png")
      state.step2_images.append(image)
      with open(image, "wb") as file:
        file.write(res.content)
      # normalize png
      Image.open(image).save(image)
      state.doProgress()
    else:
      return proc.error("Failed to retrieve page", num)
  return True
def step3_0(proc, state):
  return state.doStep(3, 2 + len(state.step2_images))
def step3_1(proc, state):
  img = Image.open(state.step2_images[0])
  state.step3_pdf = FPDF("L", "pt", [ img.size[1], img.size[0] ])
  img.close()
  state.step3_pdf.set_margins(0, 0, 0)
  return state.doProgress()
def step3_2(proc, state):
  for image in state.step2_images:
    state.step3_pdf.add_page()
    state.step3_pdf.image(image, 0, 0)
    state.doProgress()
  return True
def step3_3(proc, state):
  proc._path_pdf = os.path.join(proc.store, "output.pdf")
  state.step3_pdf.output(proc._path_pdf, "F")
  return state.doProgress()
def step4_0(proc, state):
  return state.doStep(4, 0)

STEPS = [
  step1_0, step1_1, step1_2, step1_3,
  step2_0,
  step3_0, step3_1, step3_2, step3_3,
  step4_0
]

class Processor(object):

  def error(self, error):
    self.log("error", error)
    self._error = error
    self.report()
    return False

  def report(self):
    self.owner.broadcast("update", self.getWrappedData())
    return True

  def getData(self):
    data = {
      "id": self.id,
      "name": self.name,
      "step": self.state.getStep(),
      "progress": self.state.getProgress(),
    }
    if self._error != None:
      data["error"] = str(self._error)
    if self._note != None:
      data["note"] = self._note
    if self._path_pdf != None:
      data["path_pdf"] = os.path.relpath(self._path_pdf)
    return data

  def getWrappedData(self):
    wrapper = { }
    wrapper[self.id] = self.getData()
    return wrapper

  def log(self, method, data=""):
    print("[proc][" + self.sign + "]/" + method,  data)

  def __init__(self, owner, id, request, store):
    self.owner = owner
    self.id = id
    self.request = request
    self.name = self.request.link

    self.sign = self.id[-6:]

    self.state = ProcessorState(self, STEPS)

    self._note = None
    self._error = None

    self._path_pdf = None
    self._path_raw = None

    self.store = os.path.join(store, self.id)
    self.raw = os.path.join(self.store, "raw")

    if not os.path.exists(self.store):
      os.mkdir(self.store)
    if not os.path.exists(self.raw):
      os.mkdir(self.raw)

    self.owner.server.start_background_task(runner, self)
