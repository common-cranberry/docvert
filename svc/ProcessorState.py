
import requests

class ProcessorState(object):
  def __init__(self, processor, locations):
    self.step = 1
    self.location = 0
    self.progress = 0
    self.total = 0

    self.processor = processor
    self.locations = locations

    self.session = requests.Session()
    self.headers = { "User-Agent":
        "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0" }

  def doStep(self, step, total):
    self.step = step
    self.progress = 0
    self.total = total
    return self.processor.report()

  def doProgress(self):
    self.progress += 1
    return self.processor.report()

  def doNext(self):
    try:
      fn = self.locations[self.location]
      self.processor.log(fn.__name__)
      result = fn(self.processor, self)
    except Exception as ex:
      return self.processor.error(ex)
    if result:
      self.location += 1
    return result and self.location < len(self.locations)

  def doRun(self):
    while self.doNext():
      pass
    self.processor.log("stopped")

  def getStep(self):
    return self.step

  def getProgress(self):
    if self.total != 0:
      return self.progress / self.total
    return 0
